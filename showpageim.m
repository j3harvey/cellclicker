function showpageim(handles)

global maxNumCols maxNumRows page NumPages cPitch rPitch axWidth axHight N PCs colors;

% x offset for placement of images
offset = -0.75/maxNumCols;

Npage=sprintf('%d cells - %d/%d pages',N,page,NumPages);                       % storing the number of files and pages information in Npage
set(handles.state_bar,'string',Npage);                                         % showing it in the state bar
set(handles.save,'enable','on');                                             % activate the cancel button
hActions = getappdata(handles.figure1,'actionItems');                          % get all the object stored in figure1 as 'actionItem'
set(hActions,'enable','off');                                                  % disable them to not be used during loading 
hAxes = getappdata(handles.figure1,'hAxes');                                   % Get the Axes from the figure

if ~isempty(hAxes)                                                             % if they containe sth
    f = find(ishandle(hAxes) & hAxes);
    delete(hAxes(f));                                                          % delete them
end

% gathering the properties values in two variables
axesProp = {'dataaspectratio' ,...
            'Parent',...
            'PlotBoxAspectRatio', ...
            'xgrid' ,...
            'ygrid'};
axesVal = {[1,1,1] , ...
            handles.uipanel1,...
            [1 1 1]...
            'off',...
            'off'};

track = PCs.Tracks{page};
ids = track(track > 0);
Axes_Ind=1;                                                                    % initialize the index of the Axes
for ind = ids
%while ind<=MaxIndDraw && ~get(handles.cancel,'value')
    [r, c] = ind2sub([maxNumRows maxNumCols],Axes_Ind);
    % calculating the abscissa and ordiante of the origine of the graph
    x = (c)*cPitch + offset;
    y = 1-(r)*rPitch;
    hAxes(Axes_Ind) = axes('position', [x y axWidth axHight], axesProp, axesVal);
    im = PCs.Cells(ind).PicTrans;
    imName = num2str(ind);
    plotImInAxis(im, hAxes(Axes_Ind), strcat(imName, '_trans'), 9)
    Axes_Ind=Axes_Ind+1;
    
    [r, c] = ind2sub([maxNumRows maxNumCols],Axes_Ind);
    % calculating the abscissa and ordiante of the origine of the graph
    x = (c)*cPitch + offset;
    y = 1 - (r)*rPitch;
    hAxes(Axes_Ind) = axes('position', [x y axWidth axHight], axesProp, axesVal);
    im = PCs.Cells(ind).PicGreen;
    imName = num2str(ind);
    plotImInAxis(im, hAxes(Axes_Ind), strcat(imName, '_green'), 9)
    if isfield(PCs.Cells(ind).Measurments, 'CC')
        cc = PCs.Cells(ind).Measurments.CC;
        [x, y] = size(im);
        % Draw a box to mark the current cell cc state
        if cc == -2
            rectangle('Position',[1,1,y,x], 'EdgeColor', 'b','LineWidth',2 )
            line([1 y], [x 1], 'Color', 'b','LineWidth',2 )
            line([1 y], [1 x], 'Color', 'b','LineWidth',2 )
        elseif cc == -1
            rectangle('Position',[1,1,y,x], 'EdgeColor', 'r','LineWidth',2 )
            line([1 y], [x 1], 'Color', 'r','LineWidth',2 )
            line([1 y], [1 x], 'Color', 'r','LineWidth',2 )
        elseif cc > 0
            rectangle('Position',[1,1,size(im, 2),size(im, 1)], 'EdgeColor', colors(cc),'LineWidth',2 )
        end
    end
    Axes_Ind=Axes_Ind+1;

    [r, c] = ind2sub([maxNumRows maxNumCols],Axes_Ind);
    % calculating the abscissa and ordiante of the origin of the graph
    x = (c)*cPitch + offset;
    y = 1-(r)*rPitch;
    hAxes(Axes_Ind) = axes('position', [x y axWidth axHight], axesProp, axesVal);
    im = PCs.Cells(ind).PicRed;
    imName = num2str(ind);
    plotImInAxis(im, hAxes(Axes_Ind), strcat(imName, '_red'), 9)
    if isfield(PCs.Cells(ind).Measurments, 'CP')
        cp = PCs.Cells(ind).Measurments.CP;
        [x, y] = size(im);
        % Draw a box to mark the current cell cp state
        if cp == -2
            rectangle('Position',[1,1,y,x], 'EdgeColor', 'b','LineWidth',2 )
            line([1 y], [x 1], 'Color', 'b','LineWidth',2 )
            line([1 y], [1 x], 'Color', 'b','LineWidth',2 )
        elseif cp == -1
            rectangle('Position',[1,1,y,x], 'EdgeColor', 'r','LineWidth',2 )
            line([1 y], [x 1], 'Color', 'r','LineWidth',2 )
            line([1 y], [1 x], 'Color', 'r','LineWidth',2 )
        elseif cp > 0
            rectangle('Position',[1,1,size(im, 2),size(im, 1)], 'EdgeColor', colors(cp),'LineWidth',2 )
        end
    end
    Axes_Ind=Axes_Ind+1;
end 

% enable the action items
set(hActions,'enable','on');
% update the Axes in the handle
setappdata(handles.figure1,'hAxes',hAxes)