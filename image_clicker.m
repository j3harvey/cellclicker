function varargout = image_clicker(varargin)
% IMAGE_clicker M-file for image_clicker.fig
%      IMAGE_clicker, by itself, creates a new IMAGE_clicker or raises the existing
%      singleton*.
%
%      H = IMAGE_clicker returns the handle to a new IMAGE_clicker or the handle to
%      the existing singleton*.
%
%      IMAGE_clicker('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMAGE_clicker.M with the given input arguments.
%
%      IMAGE_clicker('Property','Value',...) creates a new IMAGE_clicker or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before image_clicker_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to image_clicker_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help image_clicker

% Last Modified by GUIDE v2.5 02-Aug-2013 13:59:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @image_clicker_OpeningFcn, ...
                   'gui_OutputFcn',  @image_clicker_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes just before image_clicker is made visible.
function image_clicker_OpeningFcn(hObject, eventdata, handles, varargin)
%clc
global maxNumCols maxNumRows page N_Im_Draw NumPages...
       PCs cPitch rPitch axWidth axHight...
       CurCCStage CurCPStage colors

% initialization of the current PombeCells object
PCs = varargin{1};
for i=1:PCs.NbCells
    if ~isfield(PCs.Cells(i).Measurments, 'CC')
        PCs.Cells(i).Measurments.CC = 0;
    end
    if ~isfield(PCs.Cells(i).Measurments, 'CP')
        PCs.Cells(i).Measurments.CP = 0;
    end
end

% initialization of name of database column
CurCCStage = 0;
CurCPStage = 0;

% colors for marking CC and CP states
colors = 'rgbymcwk';

% number of pages
NumPages = length(PCs.Tracks);
% initialization of the current page
if length(varargin) > 1
    page = varargin{2};
else
    page = 1;
end

% maximal number of colomns
maxNumCols = sum(PCs.Tracks{page} > 0);
% maximal number of rows
maxNumRows = 3;
% number of images in each page
N_Im_Draw = maxNumCols;
% Abcissa of the origin
rPitch = 0.98/maxNumRows;
% ordinate of the origin
cPitch = 0.98/maxNumCols;
% width of the graph
axWidth = 0.9/maxNumCols;
% heigh of the graph
axHight = 0.9/maxNumRows;


% make the "previous" button deactivate
set(handles.prev_but,'enable','off');
% make the "next" button deactivate
set(handles.next_but,'enable','off');


set(handles.save,'enable','on');


% store GUI objects in the figure as a
setappdata(handles.figure1,'actionItems',...
                        [handles.selectCCStage,...
                        handles.selectCPStage,...
                        handles.next_but,...
                        handles.prev_but]...
           );
handles.output = hObject;
organize_image_PCs(handles)

guidata(hObject, handles);
uiwait();


% --- Outputs from this function are returned to the command line.
function varargout = image_clicker_OutputFcn(hObject, eventdata, handles) 
   global PCs
handles.output=PCs;
varargout{1} = handles.output;

    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on selection of cell shape
function selectCCStage_Callback(hObject, eventdata, handles)
global CurCCStage

str = get(hObject, 'String');
val = get(hObject,'Value');
switch str{val};
    case 'Select cell cycle stage'
        CurCCStage = 0;
    case 'wrong cell'
        CurCCStage = -2;
    case 'something else went wrong'
        CurCCStage = -1;
    case 'one nucleus'
        CurCCStage = 1;
    case 'lobed nucleus'
        CurCCStage = 2;
    case 'two nuclei'
        CurCCStage = 3;        
end




% --- Executes during object creation, after setting all properties.
function selectCCStage_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to selectCCStage (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: popupmenu controls usually have a white background o�n Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



% --- Executes on selection change in selectCPStage.
function selectCPStage_Callback(hObject, eventdata, handles)
% hObject    handle to selectCPStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectCPStage contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectCPStage
global  CurCPStage

str = get(hObject, 'String');
val = get(hObject,'Value');
switch str{val};
    case 'Select cell polarity stage'
        CurCPStage=0;
    case 'wrong cell'
        CurCPStage = -2;
    case 'something else went wrong'
        CurCPStage = -1;
    case 'monopolar 1'
        CurCPStage = 1;
    case 'bipolar 1'
        CurCPStage = 2;
    case 'monopolar 2'
        CurCPStage = 3;
    case 'bipolar 2'
        CurCPStage = 4;
    case 'septating'
        CurCPStage = 5;
    case 'septated'
        CurCPStage = 6;
    case 'X-shape'
        CurCPStage = 7;
end

% --- Executes during object creation, after setting all properties.
function selectCPStage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectCPStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in prev_but.
function prev_but_Callback(hObject, eventdata, handles)
% hObject    handle to prev_but (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global page PCs maxNumCols maxNumRows N_Im_Draw rPitch cPitch axWidth axHight;
    % go to the previous page
	page=page-1;
    
    % maximal number of colomns
    maxNumCols = sum(PCs.Tracks{page} > 0);
    % maximal number of rows
    maxNumRows = 3;
    % number of images in each page
    N_Im_Draw = maxNumCols;
    % Abcissa of the origin
    rPitch = 0.98/maxNumRows;
    % ordinate of the origin
    cPitch = 0.98/maxNumCols;
    % width of the graph
    axWidth = 0.9/maxNumCols;
    % heigh of the graph
    axHight = 0.9/maxNumRows;

    % show the pictures
    showpageim(handles)
    % if it's the first page, desactivate the button
    if page==1                                   
        set( handles.prev_but,'enable','off');
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in next_but.
function next_but_Callback(hObject, eventdata, handles)
% hObject    handle to next_but (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
	global  page NumPages PCs maxNumCols maxNumRows N_Im_Draw rPitch cPitch axWidth axHight;
    % Go to next page
	page = page + 1;

    % maximal number of colomns
    maxNumCols = sum(PCs.Tracks{page} > 0);
    % maximal number of rows
    maxNumRows = 3;
    % number of images in each page
    N_Im_Draw = maxNumCols;
    % Abcissa of the origin
    rPitch = 0.98/maxNumRows;
    % ordinate of the origin
    cPitch = 0.98/maxNumCols;
    % width of the graph
    axWidth = 0.9/maxNumCols;
    % heigh of the graph
    axHight = 0.9/maxNumRows;

    % Show the pictures
    showpageim(handles)
    % If it's the last page, deactivate the button
    if page == NumPages
           set( handles.next_but,'enable','off');
    end



% --- Executes on key press with focus on selectCCStage and none of its controls.
function selectCCStage_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to selectCCStage (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global PCs page
    track = PCs.Tracks{page};
    ids = track(track > 0);
    for i = ids
        PCs.Cells(i).Measurments.CC = 0;
        PCs.Cells(i).Measurments.CP = 0;
    end
    showpageim(handles)



% --- Executes on mouse press over figure background.
function figure1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    % image_clicker_OutputFcn(hObject, eventdata, handles)
    close(handles.figure1)
