function plotImInAxis(im, hAx, imgName, fontSize)
    
    imageProp = { 'ButtonDownFcn', 'Tag'};
    imageVal = { 'methodsSpecificImage(guidata(gcf))', imgName};
    imagesc(im,imageProp,imageVal,'parent',hAx );                               % Scale data and display image object
    colormap gray
    axis(hAx,'image');                                                          % sets the aspect ratio so that equal tick mark                                                                            % increments on the x-,y- and z-axis are equal in size.                                                                               % the plot box fits tightly around the data.
    axis(hAx,'off');                                                            % turns off all axis labeling, tick marks and background.
