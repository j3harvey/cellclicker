function methodsSpecificImage(handles)
    global PCs CurCCStage CurCPStage colors
    
    imgName = get(gcbo, 'Tag');
    ind = regexp(imgName, '[0-9]*', 'match');
    ind = str2double(ind{1,1});
    channel = regexp(imgName, '[a-z]*', 'match');
    channel = channel{1};

    x = get(gcbo, 'XData');
    y = get(gcbo, 'YData');
    
    type = get(gcf,'SelectionType');
    switch type
%         case 'open' % double-click
%             im = get( gcbo, 'cdata' );
%             imtool(im, [min(im(:)) max(im(:))] )
        case 'normal' % single click
            % redraw the cell
            im = get( gcbo, 'cdata' );
            hAx = get(gcbo, 'Parent');
            imageProp = { 'ButtonDownFcn', 'Tag'};
            imageVal = { 'methodsSpecificImage(guidata(gcf))', imgName};
            imagesc(im,imageProp,imageVal,'parent',hAx );
            colormap(gray);
            axis(hAx,'image');
            axis(hAx,'off');
            % change CC state
            if strcmp(channel, 'green')
                PCs.Cells(ind).Measurments.CC = CurCCStage;
                if CurCCStage == -2
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'b','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'b','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'b','LineWidth',2 )
                elseif CurCCStage == -1
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'r','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'r','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'r','LineWidth',2 )
                elseif CurCCStage > 0
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', colors(CurCCStage),'LineWidth',2 )
                end
            end
            
            % change CP state
            if strcmp(channel, 'red')
                PCs.Cells(ind).Measurments.CP = CurCPStage;
                if CurCPStage == -2
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'b','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'b','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'b','LineWidth',2 )
                elseif CurCPStage == -1
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'r','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'r','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'r','LineWidth',2 )
                elseif CurCPStage > 0
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', colors(CurCPStage),'LineWidth',2 )
                end
            end
        case 'extend' %shift-click
            % redraw the cell
            im = get( gcbo, 'cdata' );
            hAx = get(gcbo, 'Parent');
            imageProp = { 'ButtonDownFcn', 'Tag'};
            imageVal = { 'methodsSpecificImage(guidata(gcf))', imgName};
            imagesc(im,imageProp,imageVal,'parent',hAx );
            colormap(gray);
            axis(hAx,'image');
            axis(hAx,'off');
            
            cc = PCs.Cells(ind).Measurments.CC;
            cp = PCs.Cells(ind).Measurments.CP;
            
            % change CC state
            if strcmp(channel, 'green')
                if cc == 3
                    PCs.Cells(ind).Measurments.CC = -2;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'b','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'b','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'b','LineWidth',2 )
                elseif cc == -2
                    PCs.Cells(ind).Measurments.CC = -1;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'r','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'r','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'r','LineWidth',2 )
                elseif cc == -1
                    PCs.Cells(ind).Measurments.CC = 0;
                else
                    PCs.Cells(ind).Measurments.CC = cc + 1;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', colors(cc + 1),'LineWidth',2 )
                end
            end
            
            % change CP state
            if strcmp(channel, 'red')
                if cp == 7
                    PCs.Cells(ind).Measurments.CP = -2;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'b','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'b','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'b','LineWidth',2 )
                elseif cp == -2
                    PCs.Cells(ind).Measurments.CP = -1;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', 'r','LineWidth',2 )
                    line([1 x(2)], [1 y(2)], 'Color', 'r','LineWidth',2 )
                    line([1 x(2)], [y(2) 1], 'Color', 'r','LineWidth',2 )
                elseif cp == -1
                    PCs.Cells(ind).Measurments.CP = 0;
                elseif cp >= 0
                    PCs.Cells(ind).Measurments.CP = cp + 1;
                    rectangle('Position',[1,1,x(2),y(2)], 'EdgeColor', colors(cp + 1),'LineWidth',2 )
                end
            end
            
    end

