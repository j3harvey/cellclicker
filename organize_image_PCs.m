function organize_image_PCs(handles)
    global N page NumPages MaxIndDraw PCs;
    
    N = PCs.NbCells;
    
    showpageim(handles);
    
    % if we are in the last page  disable the next button
    if page == NumPages
        set(handles.next_but,'enable','off');
    end
    if page == 1
        set(handles.prev_but,'enable','off');
    end
